//Jawaban soal 1
const hitung = (panjang, lebar, jenis = "luas") => {
	let hasil
	if(jenis == "luas"){
		hasil = panjang * lebar
	} else if(jenis == "keliling"){
		hasil = (panjang + lebar) * 2
	}
	
	console.log("ukuran persegi panjang : " + hasil);
};
hitung(4,5 , "luas");

//Jawaban soal 2
const newFunction =(firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName : "Nama Lengkap : " + firstName + " " +lastName
    }
  }
console.log(newFunction("William", "Imoh").fullName)

//Jawaban soal 3
const firstName = "Muhammad";
const lastName = "Iqbal Mubarok";
const address = "Jalan Ranamanyar";
const hobby = "playing football";
const newObject = ["Nama : " + firstName + " " + lastName, "Alamat : " + address, "Hobby : " + hobby ];
console.log(newObject);

//Jawaban soal 4
let west = ["Will", "Chris", "Sam", "Holly"];
let east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];
console.log(combined)

//Jawaban soal 5
const planet = "earth";
const view = "glass";
console.log('Lorem ' + view + ' dolor sit amet consectetur adipiscing elit' + planet);